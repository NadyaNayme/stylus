This CSS removes nearly all GUI elements from Firefox - leaving only the URL Bar and a necessary-for-quality-of-life icons on hover of the URL Bar.

![](./preview.png)